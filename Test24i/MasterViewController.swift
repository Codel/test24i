//
//  MasterViewController.swift
//  Test24i
//
//  Created by Karel Novotný on 27/11/2019.
//  Copyright © 2019 Karel Novotný. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class MasterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var connectionHeigthConstraint: NSLayoutConstraint!
    @IBOutlet weak var serachBarBottomConstraint: NSLayoutConstraint!
    
    var detailViewController: DetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        // Notification to control search bar position.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil);
        
        refreshControl = UIRefreshControl()
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(requestGenreData), for: .valueChanged)
        
        requestGenreData()
    }

    override func viewWillAppear(_ animated: Bool) {
        // Deselect row if it necessary when get back from detail.
        if splitViewController!.isCollapsed, let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated:false)
        }
        super.viewWillAppear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // Notification to control search bar position.
    @objc func keyboardChange(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                serachBarBottomConstraint.constant = 0.0
            } else {
                serachBarBottomConstraint.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: { self.view.layoutIfNeeded() }, completion: nil)
        }
    }
    
    // Show or hide No connection notice
    func connectionNotice(show: Bool) {
        if show {
            connectionHeigthConstraint.constant = 44
        } else {
            connectionHeigthConstraint.constant = 0
        }
        UIView.animate(withDuration: 0.5, delay: TimeInterval(0), options: .curveEaseInOut, animations: { self.view.layoutIfNeeded() }, completion: nil)
    }
    
    // MARK: - Genre data
    
    @objc func requestGenreData() {
        AF.request("\(API_URL)\(API_URL_GENRE)?\(API_PARAM_KEY)=\(API_KEY)&\(API_PARAM_LANGUAGE)=\(Locale.current.languageCode ?? "")")
            .validate(statusCode: 200...200)
            .validate(contentType: [CONTENT_TYPE_JSON])
            .responseJSON { response in
                switch response.result {
                    case .success:
                        if let responseValue = response.value as? Dictionary<String, Any> {
                            self.connectionNotice(show: false)
                            self.processGenreData(responseValue: responseValue)
                        } else {
                            self.connectionNotice(show: true)
                        }

                    case .failure:
                        self.connectionNotice(show: true)
                }
                self.refreshControl.endRefreshing()
            }
    }
    
    func processGenreData(responseValue: Dictionary<String, Any>) {
        let fetchRequest: NSFetchRequest<Genre> = Genre.fetchRequest()
        fetchRequest.fetchLimit = 1

        do {
            for genreData in responseValue["genres"] as? [[String: Any]] ?? [] {
                var genre: Genre
                
                fetchRequest.predicate = NSPredicate(format: "id = \(genreData["id"] as? NSNumber ?? 0)")
                let genreList = try managedObjectContext!.fetch(fetchRequest)
                if genreList.count > 0 {
                    genre = genreList[0]
                } else {
                    genre = Genre(context: managedObjectContext!)
                }
                genre.setFrom(genreData: genreData)
            }

            try managedObjectContext!.save()
            requestMovieData()
        } catch {
        }
    }
    
    // MARK: - Movie data

    func requestMovieData() {
        AF.request("\(API_URL)\(API_URL_MOVIE)\(API_URL_POPULAR)?\(API_PARAM_KEY)=\(API_KEY)&\(API_PARAM_LANGUAGE)=\(Locale.current.languageCode ?? "")")
            .validate(statusCode: 200...200)
            .validate(contentType: [CONTENT_TYPE_JSON])
            .responseJSON { response in
                switch response.result {
                    case .success:
                        if let responseValue = response.value as? Dictionary<String, Any> {
                            self.connectionNotice(show: false)
                            self.processMovieData(responseValue: responseValue)
                        } else {
                            self.connectionNotice(show: true)
                        }
                        
                    case .failure:
                        self.connectionNotice(show: true)
                }
            }
    }

    func processMovieData(responseValue: Dictionary<String, Any>) {        
        do {
            fetchedResultsController.delegate = nil
            setDeleteMovies() // Set delete flag to all movies for detect non popular movies.

            for movieData in responseValue["results"] as? [[String: Any]] ?? [] {
                if let movie = getMovie(id: movieData["id"] as! NSNumber) {
                    movie.setFrom(movieData: movieData)
                }
            }
            try managedObjectContext!.save()

            fetchedResultsController.delegate = self
            deleteMovies() // Delete old movies, which is not in new list.
            requestPosterData()
        } catch {
        }
    }
    
    func getMovie(id: NSNumber) -> Movie? {
        var movie: Movie?
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = NSPredicate(format: "id = \(id)")
        
        do {
            let movieList = try managedObjectContext!.fetch(fetchRequest)
            if movieList.count > 0 {
                movie = movieList[0]
            } else {
                movie = Movie(context: managedObjectContext!)
            }
        } catch {
        }

        return movie
    }
    
    // Set delete flag to all movies for detect non popular movies.
    func setDeleteMovies() {
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()

        do {
            let movieList = try managedObjectContext!.fetch(fetchRequest)
            for movie in movieList {
                movie.delete = true
            }
        } catch {
        }
    }
    
    // Delete old movies, which is not in new list.
    func deleteMovies() {
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()

        do {
            let movieList = try managedObjectContext!.fetch(fetchRequest)
            for movie in movieList {
                if movie.delete {
                    managedObjectContext!.delete(movie)
                }
            }
        } catch {
        }
    }
    
    // MARK: - Poster data

    func requestPosterData() {
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()

        do {
            let movieList = try managedObjectContext!.fetch(fetchRequest)
            for movie in movieList {
                if movie.poster == nil {
                    requestPosterData(movie: movie)
                }
            }
        } catch {
        }
    }
    
    func requestPosterData(movie: Movie) {
        AF.request("\(API_URL_POSTER)\(movie.posterPath ?? "")")
            .validate(statusCode: 200...200)
            .validate(contentType: [CONTENT_TYPE_JPEG])
            .responseData { response in
                switch response.result {
                    case .success:
                        do {
                            movie.poster = response.value
                            try self.managedObjectContext!.save()
                        } catch {
                        }

                    case .failure:
                        self.connectionNotice(show: true)
                }
            }
    }
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let movie = fetchedResultsController.object(at: indexPath)
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.movie = movie
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                detailViewController = controller
            }
        }
    }

    // MARK: - Table View

    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableCell", for: indexPath) as! MovieTableCell
        let movie = fetchedResultsController.object(at: indexPath)

        cell.movie = movie

        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController<Movie> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()
        
        fetchRequest.fetchBatchSize = 1
        
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil) // Disable cache because iOS 10 bug
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<Movie>? = nil

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
            case .insert:
                tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
            case .delete:
                tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
            default:
                return
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
            case .insert:
                tableView.insertRows(at: [newIndexPath!], with: .fade)
            case .delete:
                tableView.deleteRows(at: [indexPath!], with: .fade)
            case .update:
                tableView.reloadRows(at: [indexPath!], with: .none)
            case .move:
                tableView.moveRow(at: indexPath!, to: newIndexPath!)
            default:
                return
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    // MARK: - Search bar
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchMovie(title: searchText)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchMovie(title: "")
    }

    func searchMovie(title: String) {
        if title.count > 0 {
            fetchedResultsController.fetchRequest.predicate = NSPredicate(format: "title CONTAINS [cd] %@", title)
        } else{
            fetchedResultsController.fetchRequest.predicate = nil
        }
        
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
        }
    }
}
