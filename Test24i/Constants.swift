//
//  Constants.swift
//  Test24i
//
//  Created by Karel Novotný on 29/11/2019.
//  Copyright © 2019 Karel Novotný. All rights reserved.
//

let API_URL             = "https://api.themoviedb.org"
let API_KEY             = "173bb69c3b47c17d111c6d3b3deeadb9"
let API_URL_GENRE       = "/3/genre/movie/list"
let API_URL_MOVIE       = "/3/movie/"
let API_URL_POPULAR     = "popular"
let API_URL_TRAILER     = "/videos"
let API_URL_POSTER      = "https://image.tmdb.org/t/p/w342"
let API_PARAM_KEY       = "api_key"
let API_PARAM_LANGUAGE  = "language"

let CONTENT_TYPE_JSON   = "application/json"
let CONTENT_TYPE_JPEG   = "image/jpeg"
