//
//  Genre+CoreDataClass.swift
//  Test24i
//
//  Created by Karel Novotný on 02/12/2019.
//  Copyright © 2019 Karel Novotný. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Genre)
public class Genre: NSManagedObject {
    
    func setFrom(genreData: [String: Any]) {
        id = genreData["id"] as? NSNumber
        name = genreData["name"] as? String ?? ""
    }
}
