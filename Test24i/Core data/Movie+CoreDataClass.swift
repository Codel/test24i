//
//  Movie+CoreDataClass.swift
//  Test24i
//
//  Created by Karel Novotný on 02/12/2019.
//  Copyright © 2019 Karel Novotný. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Movie)
public class Movie: NSManagedObject {

    func setFrom(movieData: [String: Any]) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let fetchRequest: NSFetchRequest<Genre> = Genre.fetchRequest()
        fetchRequest.fetchLimit = 1

        id = movieData["id"] as? NSNumber
        title = movieData["title"] as? String ?? ""
        releaseDate = dateFormatter.date(from:movieData["release_date"] as? String ?? "")!
        overview = movieData["overview"] as? String ?? ""
        posterPath = movieData["poster_path"] as? String ?? ""
        delete = false
        
        genres = []
        for genreId in movieData["genre_ids"] as? [NSNumber] ?? [] {
            fetchRequest.predicate = NSPredicate(format: "id = \(genreId)")
            do {
                let genreList = try managedObjectContext!.fetch(fetchRequest)
                if genreList.count > 0 {
                    addToGenres(genreList[0])
                }
            } catch {
            }
        }
    }
}
