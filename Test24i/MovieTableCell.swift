//
//  MovieTableCell.swift
//  Test Novotny
//
//  Created by Karel Novotný on 26/11/2019.
//  Copyright © 2019 Karel Novotný. All rights reserved.
//

import UIKit

class MovieTableCell: UITableViewCell {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var movie: Movie? = nil {
        didSet {
            if let movie = movie {
                if let poster = movie.poster {
                    imgPoster.image = UIImage(data: poster)
                }  else {
                    imgPoster.image = nil
                }
                lblTitle.text = movie.title
            }
        }
    }
}
