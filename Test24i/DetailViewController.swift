//
//  DetailViewController.swift
//  Test24i
//
//  Created by Karel Novotný on 27/11/2019.
//  Copyright © 2019 Karel Novotný. All rights reserved.
//

import UIKit
import AVKit
import Alamofire

struct YouTubeVideoQuality {
    static let hd720 = NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)
    static let medium360 = NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)
    static let small240 = NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)
}

class DetailViewController: UIViewController {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var btnWatchTrailer: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenres: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet var allComponents: [UIView]!
    @IBOutlet weak var watchTrailerHeigthConstraint: NSLayoutConstraint!

    var youtubeKey: String = ""
    
    @IBAction func btnWatchTrailerClick(_ sender: UIButton) {
        let playerViewController = AVPlayerViewController()
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object:  playerViewController.player?.currentItem)
        self.present(playerViewController, animated: true)

        XCDYouTubeClient.default().getVideoWithIdentifier(youtubeKey) { [weak playerViewController] (video: XCDYouTubeVideo?, error: Error?) in
            if let streamURLs = video?.streamURLs, let streamURL = (streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs[YouTubeVideoQuality.hd720] ?? streamURLs[YouTubeVideoQuality.medium360] ?? streamURLs[YouTubeVideoQuality.small240]) {
                playerViewController?.player = AVPlayer(url: streamURL)
                playerViewController?.player?.play()
            } else {
                self.dismiss(animated: true)
            }
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        dismiss(animated: true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func configureView() {
        // If all components are initialized
        if allComponents != nil {
            if let movie = movie {
                setComponentsHidden(false)

                lblTitle.text = movie.title
                lblOverview.text = movie.overview

                hideShowWatchTrailer()
                
                if let poster = movie.poster {
                    imgPoster.image = UIImage(data: poster)
                }  else {
                    imgPoster.image = nil
                }

                var genreArray: [String] = []
                if let genres = movie.genres {
                    for i in 0..<genres.count {
                        if let name = (genres[i] as! Genre).name {
                            genreArray.append(name)
                        }
                    }
                }
                lblGenres.text = genreArray.joined(separator: ", ")
                
                if let releaseDate = movie.releaseDate {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateStyle = .medium
                    dateFormatter.timeStyle = .none
                    dateFormatter.locale = Locale.current
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)

                    lblDate.text = dateFormatter.string(from: releaseDate)
                } else {
                    lblDate.text = ""
                }
            } else {
                setComponentsHidden(true)
            }
        }
    }
    
    func hideShowWatchTrailer(){
        DispatchQueue.main.async {
            self.btnWatchTrailer.isHidden = true
            self.watchTrailerHeigthConstraint.constant = 0
            AF.request("\(API_URL)\(API_URL_MOVIE)\(self.movie?.id ?? 0)\(API_URL_TRAILER)?\(API_PARAM_KEY)=\(API_KEY)&\(API_PARAM_LANGUAGE)=\(Locale.current.languageCode ?? "")")
                .validate(statusCode: 200...200)
                .validate(contentType: [CONTENT_TYPE_JSON])
                .responseJSON { response in
                    switch response.result {
                        case .success:
                            // If server respond and result contains video key, show button.
                            if let responseValue = response.value as? Dictionary<String, Any>, let movieList = responseValue["results"] as? [[String: Any]], movieList.count > 0, let key = movieList[0]["key"] as? String {
                                self.youtubeKey = key
                                self.btnWatchTrailer.isHidden = false
                                self.watchTrailerHeigthConstraint.constant = 44
                            }

                        case .failure:
                            break
                    }
                }
        }
    }
    
    func setComponentsHidden(_ hidden: Bool) {
        for view in allComponents {
            view.isHidden = hidden
        }
    }
    
    var movie: Movie? {
        didSet {
            configureView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
}
